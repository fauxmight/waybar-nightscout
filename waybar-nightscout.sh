#!/usr/bin/env bash
#
# waybar-nightscout.sh - display most recent blood glucose reading from an operating nightscout site
#
# If "enable_gnuplot" is set to true, then the tooltip will display a tui plot of recent values
#  - gnuplot doesn't handle timezones, so "gnuplot_millisecond_offset" is used to change the time for your timezone
#    ONE hour == 3600000 milliseconds
#  - gnuplot_width  is the width  of the tooltip plot in characters
#  - gnuplot_height is the height of the tooltip plot in characters
#  - gnuplot_bg_scale_max is the Y-value (blood glucose) max for the plot
#  - gnuplot_datapoints_count is the number of datapoints to pull from nightscout (must be >= 10, else it will be set to 10)
#  - table_fallback allows the same configuration to be used in multiple systems
#                   if gnuplot is in $PATH, it is used; else a table of recent values is constructed
#
# In waybar's config.json file, create a custom entry like
#        "custom/nightscout": {
#            "exec":                    "/xap/etc/waybar/waybar-nightscout.sh",
#            "format":                  "{}",
#	     "interval":                60,
#	     "return-type":             "json",
#        },
#
# Hint: put the full "?token=....." portion of appended URL in your .authinfo.gpg file (to keep the token out of this script)
#       Example .authinfo.gpg line: "machine my.nightscout-domain.com login NULL password ?token=waybar-74d74a881f7a2a85"
#
# If this is too complex for you or too much effort, you can set nightscout_token to the quoted raw token string,
#    but be careful where this file is then stored. Your token can easily be made public knowledge.


######### MODIFY HERE #########
gnuplot_enabled=true
gnuplot_width=100
gnuplot_height=55
gnuplot_bg_scale_max=300
gnuplot_millisecond_offset=-18000000 # cdt
#gnuplot_millisecond_offset=-21600000 # cst
gnuplot_datapoints_count=25
table_fallback=true
nightscout_sgv_url="https://$(gpg2 -q --for-your-eyes-only --no-tty -d ~/.authinfo.gpg | awk '/nightscout/ {print $2}')/api/v1/entries/sgv"
nightscout_token=$(gpg2 -q --for-your-eyes-only --no-tty -d ~/.authinfo.gpg | awk '/nightscout/ {print $NF}')
low_mark=70
high_mark=170
stale_reading_minutes=10
pango_style_default="<span        foreground='#ebdbb2'>" # value "normal"
pango_style_attention="<span      foreground='#fb4934'>" # value too high/low
pango_style_stale="<span          foreground='#928374'>" # value too old
pango_style_tooltip_urgent="<span foreground='#fb4934'>" # value too high/low (tooltip)
######### MODIFY HERE #########

# If timestamps differ by more than this, at least one reading was missed
# "Normal" interval is five minutes (30000 seconds)
# Nightscout reports timestamps in milliseconds
max_normal_interval=360000

# Generate trend indicator character
get_trend_char () {
    case "${1}" in
	"\"DoubleUp\"")      echo "⇑";;
	"\"SingleUp\"")      echo "↑";;
	"\"FortyFiveUp\"")   echo "∕";;
	"\"Flat\"")          echo "→";;
	"\"FortyFiveDown\"") echo "∖";;
	"\"SingleDown\"")    echo "↓";;
	"\"DoubleDown\"")    echo "⇓";;
	"-")                 echo "-";;
    esac
}

now_timestamp=$(date +%s)

# Generate a series of dashes to fill all values when failing to pull valid info from Nightscout
for i in `seq 0 9`; do error_values_string+="- - - - -\n"; done

# If gnuplot_datapoints_count < 10, set it to 10 to allow the data pull to occur
[[ ${gnuplot_datapoints_count} < 10 ]] && gnuplot_datapoints_count=10

# This is the important pull of data from the server. All else should use this data. 
nightscout_raw_data=$(curl "${nightscout_sgv_url}${nightscout_token}&count=${gnuplot_datapoints_count}" 2>/dev/null || echo -e "${error_values_string}")

# Fill each array (prettydate, timestamp, bg, trend, xdripsource) with 10 values pulled from Nightscout API
for i in `seq 0 9`; do read prettydate[$i] timestamp[$i] bg[$i] trend[$i] xdripsource[$i]; done \
    <<< "${nightscout_raw_data}"

# Generate "minutes ago" for each bg reading
now_timestamp=$(date +%s)
for i in `seq 0 9`; do
    # timestamp will ONLY be "-" if there is trouble reading network data
    if [[ ${timestamp[$i]} == "-" ]]; then
	minutes_ago[$i]=" -"
	continue
    fi
    minutes_ago[$i]=$(( ( ${now_timestamp} - ${timestamp[$i]::10} ) / 60 ))
    [[ ${minutes_ago[$i]} -lt 100 ]] && minutes_ago[$i]=" ${minutes_ago[$i]}" # space-pad double-digit numbers
    [[ ${minutes_ago[$i]} -lt 10  ]] && minutes_ago[$i]=" ${minutes_ago[$i]}" # space-pad single-digit numbers
done

# Generate bg_delta for latest 9 readings
bg_delta[9]=" --"
for i in `seq 8 -1 0`; do
    # If there is trouble reading network data OR if timestamps indicate missed readings, "bg_delta" will be " --"
    if [[ ${bg[$i]} == "-" || $(( timestamp[$i] - timestamp[$i+1])) -gt max_normal_interval ]]; then
	bg_delta[$i]=" --"
    else
	bg_delta[$i]=$(( $"bg[$i]" - $"bg[$i+1]" ))
	[[ "${bg_delta[$i]}" -gt   9                             ]] && bg_delta[$i]="+${bg_delta[$i]}"  && continue
	[[ "${bg_delta[$i]}" -gt  -1 && "${bg_delta[$i]}" -lt 10 ]] && bg_delta[$i]=" +${bg_delta[$i]}" && continue
	[[ "${bg_delta[$i]}" -gt -10 && "${bg_delta[$i]}" -lt  0 ]] && bg_delta[$i]=" ${bg_delta[$i]}"  && continue
	[[ "${bg_delta[$i]}" -lt  -9                             ]] && bg_delta[$i]="${bg_delta[$i]}"   && continue
    fi
done

# Apply pango_style_tooltip_urgent to bg readings that are out of range
for i in `seq 0 9`; do
    if [[ "${bg[$i]}" == "-" || ( "${bg[$i]}" -gt "${low_mark}" && "${bg[$i]}" -lt "${high_mark}" ) ]]; then
	tooltip_pango_wrapped_bg[$i]="${bg[$i]}"
    else
	tooltip_pango_wrapped_bg[$i]="${pango_style_tooltip_urgent}${bg[$i]}</span>"
    fi
done

# If gnuplot is not in $PATH, and $table_fallback==true fall back to the "table of recent values" tooltip
! which gnuplot > /dev/null && ${table_fallback} && gnuplot_enabled=false

if ${gnuplot_enabled}; then
	# Tooltip is a gnuplot text rendered glucose curve if $gnuplot_enabled == true; else a table of recent values
	tooltip_text=$(echo "${nightscout_raw_data}" |
					   awk -v offset=${gnuplot_millisecond_offset} '{print substr(($2+offset),1,10) "\t" $3}' |
					   gnuplot -p -e 'set terminal block braille size '${gnuplot_width}', '${gnuplot_height}'; set autoscale; set yrange [0:'${gnuplot_bg_scale_max}']; set xdata time; set timefmt "%s"; set format x "%H:%M"; set grid; plot "-" using 1:2 with points pt "*" notitle')
	tooltip_text="${tooltip_text//$'\n'/\\n}" # replace newlines with '\n'
else
	# Compose the tooltip text (series of 10 "ago"s, 10 deltas and 10 associated bg readings)
	tooltip_text="Min ago │   Δ   │ BG mg/dL\n"
	tooltip_text+="──────────────────────────\n"
	for i in `seq 9 -1 0`; do
		tooltip_text+="    ${minutes_ago[$i]} │ ${bg_delta[$i]} $(get_trend_char ${trend[$i]}) │ ${tooltip_pango_wrapped_bg[$i]}\n"
	done
	tooltip_text+="──────────────────────────\n"
	tooltip_text+="    $( date +"%d %b @ %H:%M:%S" -d @"${now_timestamp}")"
fi

# wrap ${bg[0]} in ${pango_style_stale}     if the reading is more than $stale_reading_minutes old OR bg == "-" (trouble reading network data)
# wrap ${bg[0[} in ${pango_style_attention} if the reading is not between low_mark and high_mark
if [[ ${bg[0]} == "-" || ${minutes_ago[0]} -gt $stale_reading_minutes ]]; then
    printf -- '{"text":"%s","tooltip":"%s"}\n'     "${pango_style_stale} ${bg[0]}$(get_trend_char ${trend[0]})</span>" "${tooltip_text}"
elif [[ ${bg[0]} -gt ${low_mark} && ${bg[0]} -lt ${high_mark} ]]; then
    printf -- '{"text":"%s","tooltip":"%s"}\n'   "${pango_style_default} ${bg[0]}$(get_trend_char ${trend[0]})</span>" "${tooltip_text}"
else
    printf -- '{"text":"%s","tooltip":"%s"}\n' "${pango_style_attention} ${bg[0]}$(get_trend_char ${trend[0]})</span>" "${tooltip_text}"
fi
