# waybar-nightscout.sh

waybar-nightscout.sh pulls info from an active [nightscout](https://nightscout.github.io/) installation and outputs that info for consumption by [waybar](https://github.com/Alexays/Waybar)
----------------------------------------------------------------------------------------------------------------------------------------

- See waybar-nightscout.sh comments for usage details
